import React, { Component } from 'react';
import './App.css';

const Main_Nav = props => {
    return (
        <nav className="main-nav">
            <ul className="nav">
                <li><a href="#">{props.art}</a></li>
                <li><a href="#">{props.home}</a></li>
                <li><a href="#">{props.jewelry}</a></li>
                <li><a href="#">{props.women}</a></li>
                <li><a href="#">{props.men}</a></li>
                <li><a href="#">{props.kids}</a></li>
                <li><a href="#">{props.vintage}</a></li>
                <li><a href="#">{props.weddings}</a></li>
                <li><a href="#">{props.more}</a></li>
            </ul>
        </nav>
    );
};

const Footer = props => {
    return (
        <div className="footer">
            <ul className="footer-nav">
                <li><a href="#">{props.email}</a></li>
                <li><a href="#">{props.telnumber}</a></li>
                <li><a href="#">{props.studio}</a></li>
            </ul>
        </div>
    )
};

class App extends Component {
    render (){
        return (
            <div>
                <Main_Nav
                    art="Art"
                    home="Home"
                    jewelry="Jewelry"
                    women="Women"
                    men="Men"
                    kids="vintage"
                    vintage="Vintage"
                    weddings="Weddings"
                    more="More"
                />

                <Footer
                    email="Email: kochkarof@gmail.com"
                    telnumber="Tel: 0550606655"
                    studio="Created by WWW"
                />
            </div>

        )
    }

}

export default App;